const Genius = require('../')
const { token } = require('./auth.json')
const client = new Genius.Client(token)

// Louie Zong
client.getArtist(1387192)
  .then(res => console.log(res))

// Get Louie's Songs
client.getSongByArtist(1387192)
  .then(res => console.log(res))
