const Genius = require('../')
const { token } = require('./auth.json')
const client = new Genius.Client(token)

client.searchAPI('Hello World')
  .then(searchRes => {
    console.log('First Result', searchRes.result[0])
    client.scrapeLyrics(searchRes.result[0].url)
      .then(scraped => console.log('Scraped Lyrics', scraped))
  })
  .catch(err => console.err(err))

client.searchAPI('https://genius.com/Bill-wurtz-at-the-airport-terminal-lyrics')
  .then(searchRes => {
    console.log('Search from URL', searchRes.result[0])
  })
