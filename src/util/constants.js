/**
 * List of Genius Artist that is not an actual artist
 */
exports.ExcludedArtists = [
  'Genius',
  'Rap Genius',
  'Lit Genius',
  'Genius France',
  'Genius Brasil',
  'Sports Genius',
  'Screen Genius',
  'Genius Lists',
  'Genius Users',
  'Genius Annotation',
  'Fashion Genius',
  'Emoji Genius',
  'Game Genius',
  'Sport Genius Football',
  'Genius Engineering Team',
  'K-Pop Genius',
  'Comms @ Genius',
  'Genius Editors',
  'Education Genius',
  'Rap Genius Users',
  'Genius Russia',
  'Genius Dream Album'
]

/**
 * Default options for getSongByArtist()
 * @typedef {Object} getSongByArtistOptions
 * @property {'popularity'|'title'} sort How should the result be sorted
 * @property {Number} per_page Amount of song that should be returned
 */
exports.getSongByArtistDefaultOpts = {
  sort: 'popularity',
  per_page: 20
}
