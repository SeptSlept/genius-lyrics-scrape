const scrapeLyrics = require('../client/functions/scrapeLyrics.js')
const excludedArtists = require('../util/constants.js').ExcludedArtists
const Artist = require('./Artist.js')

/**
 * Represents a song on Genius
 */
class Song {
  /**
   * Constructs
   * @param {GeniusHitsResult} result Genius API Hits
   * @constructor
   */
  constructor (result) {
    // console.log(hits)
    if (excludedArtists.every(e => result.primary_artist.name !== e)) this._setup(result)
  }

  /**
   * Class setup function
   * @param {GeniusHitsResult} hits Genius Hits
   * @private
   * @returns {void}
   */
  _setup (result) {
    /**
     * The ID of the song
     * @type {Number}
     */
    this.songID = result.id

    /**
     * The complete song title, followed with artists and featuring artists.
     * Formatted as `<Song Title> by <Artist> (Ft. <Featuring Artists>)`
     * @example 'Troublemaker (Hello World) by Lil Wayne (Ft. Young Money)'
     * @type {String}
     */
    this.fullTitle = result.full_title

    /**
     * The title of the song
     * @type {String}
     */
    this.title = result.title

    /**
     * The song title followed by featuring artists.
     * Formatted as `<Song Title> (Ft. <Featuring Artists>)`
     * @type {String}
     */
    this.titleFeatured = result.title_with_featured

    /**
     * The primary artists details
     * @type {Artist}
     */
    this.primaryArtist = new Artist(result.primary_artist)

    /**
     * The URL of the song art
     * @type {String}
     */
    this.songArt = result.song_art_image_url

    /**
     * The URL of the song's header image on Genius
     * @type {String}
     */
    this.headerImage = result.header_image_url

    /**
     * The URL of the Genius page
     * @type {String}
     */
    this.url = result.url
  }

  /**
   * Finds the lyrics of the Song
   * @async
   * @returns {Promise<String>}
   */
  get scrapeLyrics () {
    return scrapeLyrics(this.url)
  }
}

module.exports = Song

/**
 * The Genius API search result
 * @typedef {Object} GeniusHits
 * @property {Array} highlights
 * @property {'song'} index
 * @property {'song'} type - Type of hits
 * @property {GeniusHitsResult} result - Result object
 */

/**
 * @typedef {Object} GeniusHitsResult
 * @property {Number} annotation_count - The amount of annotation in the lyrics (When highlighted, more info about the part of the lyrics is displayed)
 * @property {String} api_path - The API path of the hit
 * @property {String} full_title - The full title of the hit.If song, full_title includes primary artist and featuring artist
 * @property {String} header_image_thumbnail_url - The URL to the header image thumbnail of the hit page
 * @property {String} header_image_url - The URL to the header image of the hit page
 * @property {Number} id - The Genius ID of the hit
 * @property {Number} lyrics_owner_id - The ID of the Genius User who created the hit page
 * @property {'complete'|'partial'} lyrics_state - The state of the lyrics page
 * @property {String} path - The path to the page when using `https://genius.com`
 * @property {Number} pyong_count - The number of pyongs in the page
 * @property {String} song_art_image_thumbnail_url - The URL to the song art thumbnail
 * @property {String} song_art_image_url - The URL to the song art
 * @property {Object} stats - The meta status of the page
 * @property {Number} stats.unreviewed_annotations - The number of annotations that is unreviewed
 * @property {Boolean} stats.hot - True if the page is hot (trending)
 * @property {Number} stats.pageviews - The number of views the page have
 * @property {String} title - The title of the hit
 * @property {String} title_with_featured - The title of the song including featuring artist. `Hello World (Ft. Louie Zong)`
 * @property {String} url - The URL of the page (using `https://genius.com`)
 * @property {GeniusPrimaryArtist} primary_artist - The primary artist of the song
 */

/**
 * @typedef {Object} GeniusPrimaryArtist
 * @property {String} api_path - The Genius API Path of the artist
 * @property {String} header_image_url - The URL to the header image of the artist
 * @property {Number} id - The Genius ID of the artist
 * @property {String} image_url - The URL of the artist's profile picture
 * @property {Boolean} is_meme_verified
 * @property {Boolean} is_verified - True if the Artist is verified
 * @property {String} name - The name of the Artist
 * @property {String} url - The URL of the Artist (using `https://genius.com`)
 */
