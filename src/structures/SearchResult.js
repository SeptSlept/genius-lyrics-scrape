const Song = require('./Song.js')

// Structures
/* eslint-disable */
const searchAPI = require('../client/functions/searchAPI.js')
/* eslint-enable */

/**
 * Represents Genius' search result
 */
class SearchResult {
  /**
   * Constructs SearchResult class
   * @constructor
   * @param {searchAPI.GeniusAPIRes.response} data
   */
  constructor (data) {
    if (data) this._setup(data)
  }

  /**
   * Class setup function
   * @param {searchAPI.GeniusAPIRes.response} data Response from Genius API
   * @private
   */
  _setup (data) {
    /**
     * The result of the search
     * @type {Song[]}
     */
    this.result = []

    data = data.hits

    data.forEach((v) => {
      if (v.type !== 'song') return

      this.result.push(new Song(v.result))
    })
  }
}

module.exports = SearchResult
