/**
 * Represents an artist profile on Genius
 */
class Artist {
  /**
   * Constructs an Artist class
   * @constructor
   * @param {Object} artistData Artist Object from Genius API
   */
  constructor (artistData) {
    if (typeof artistData.songs === 'undefined') { artistData.songs = [] }

    this._setup(artistData)
  }

  _setup (artistData) {
    /**
     * The ID for the artist
     * @type {Number}
     */
    this.id = artistData.id

    /**
     * The name of the artist
     * @type {String}
     */
    this.name = artistData.name

    /**
     * The URL of the artist avatar on Genius
     * @type {String}
     */
    this.avatar = artistData.image_url

    /**
     * Whether this artist page is verified
     * @type {Boolean}
     */
    this.verified = artistData.is_verified

    if (typeof artistData.alternate_names === 'undefined') artistData.alternate_names = []
    artistData.alternate_names.unshift(this.name)
    /**
     * Artist alternative name (aliases) including their primary name
     * @type {String[]}
     */
    this.names = artistData.alternate_names

    /**
     * The Artist's follower count on Genius
     * @type {Number|null}
     */
    this.followerCount = artistData.follower_count

    /**
     * Whether this Artist page is a Genius meme
     * @type {Boolean}
     */
    this.meme = artistData.is_meme_verified

    /**
     * Genius URL of the artist
     * @type {String}
     */
    this.url = artistData.url

    /**
     * Artist's social media usernames
     * @type {Object}
     */
    this.social = {
      /**
       * Artist's Facebook username
       * @type {String|null}
       */
      facebook: artistData.facebook_name,
      /**
       * Artist's Instagram username
       * @type {String|null}
       */
      instagram: artistData.instagram_name,
      /**
       * Artist's Twitter username
       * @type {String|null}
       */
      twitter: artistData.twitter_name
    }
  }
}

module.exports = Artist
