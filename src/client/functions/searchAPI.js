const phin = require('phin')
const SearchResult = require('../../structures/SearchResult.js')

// Structures
/* eslint-disable */
const Song = require('../../structures/Song')
/* eslint-enable */

/**
 * Searches Genius API for query. This also works for Genius URL query
 * @async
 * @param {String} input Query
 * @param {String} token Genius API token
 * @returns {Promise<SearchResult>} Search result
 */
function searchAPI (input, token) {
  return new Promise((resolve, reject) => {
    // String and URL
    let query
    try { // URL
      const url = new URL(input)
      query = url.pathname.substr(1).split('-')
      query.pop()
      query = query.join(' ')
    } catch {
      query = input
    }

    const querySanitized = encodeURI(query)

    phin({
      url: `https://api.genius.com/search?q=${querySanitized}`,
      headers: { Authorization: `Bearer ${token}` },
      parse: 'json'
    })
      .then((res) => {
        /**
         * Axios result
         * @type {GeniusAPIRes}
         */
        const data = res.body
        const response = data.response

        if (res.status >= 400) {
          const errorMsg = data.status + ': ' + data.message
          reject(errorMsg)
        }

        const searchResult = new SearchResult(response)
        resolve(searchResult)
      })
  })
}

module.exports = searchAPI

/**
 * @typedef {Object} GeniusAPIRes
 * @property {Object} meta
 * @property {Object} meta.status - Status of query
 * @property {Object} response
 * @property {Song.GeniusHits} response.hits - Genius Hits
 */
