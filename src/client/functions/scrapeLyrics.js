const phin = require('phin')
const cheerio = require('cheerio')

/**
 * Scrape lyrics from Genius
 * @param {String} url The URL of the Genius Page
 * @returns {Promise<String>} Lyrics
 */
function scrapeLyrics (url) {
  return new Promise((resolve, reject) => {
    url = encodeURI(url)

    phin({
      url: url
    })
      .then((rawFetch) => {
        rawFetch = rawFetch.body

        try {
          // Load full page to Cheerio
          let $ = cheerio.load(rawFetch)
          const pageData = $('meta[itemprop=page_data]').attr('content')
          const pageDataJSON = JSON.parse(pageData)

          // Load Lyrics to Cheerio
          $ = cheerio.load(pageDataJSON.lyrics_data.body.html)
          const lyrics = cheerio.text($('body'))

          resolve(lyrics)
        } catch (err) {
          reject(err)
        }
      })
      .catch(err => reject(err))
  })
}

module.exports = scrapeLyrics
