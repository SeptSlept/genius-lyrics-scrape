const phin = require('phin')
const Song = require('../../structures/Song.js')

/**
 * Gets song by Genius ID
 * @param {Number} id The Genius ID of the song
 * @param {String} token The Genius Token
 * @returns {Song} Song
 */
function getSong (id, token) {
  return new Promise((resolve, reject) => {
    phin({
      url: `https://api.genius.com/songs/${id}`,
      headers: { Authorization: 'Bearer ' + token },
      parse: 'json'
    }).then(res => {
      const data = res.body
      if (data.meta.status > 200) return reject(new Error(data.meta.message))
      return resolve(new Song(res.body.response.song))
    }).catch(err => {
      return reject(new Error(err))
    })
  })
}

module.exports = getSong
