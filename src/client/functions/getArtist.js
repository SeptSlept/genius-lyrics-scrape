const phin = require('phin')
const Artist = require('../../structures/Artist.js')

/**
 * Gets Artist
 * @param {Number} id The ID of the Artist
 * @param {String} token The bot token
 */
function getArtist (id, token) {
  return new Promise((resolve, reject) => {
    phin({
      method: 'GET',
      url: `https://api.genius.com/artists/${id}`,
      headers: { Authorization: 'Bearer ' + token },
      parse: 'json'
    })
      .then(res => {
        const artistData = res.body.response.artist
        const artist = new Artist(artistData)

        resolve(artist)
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports = getArtist
