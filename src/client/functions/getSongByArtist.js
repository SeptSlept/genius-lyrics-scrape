const phin = require('phin')
const Song = require('../../structures/Song.js')

// Structures
/* eslint-disable */
const constants = require('../../util/constants')
/* eslint-enable */

/**
 * Gets the list of song created by the artist
 * @param {Number} id The artist's Genius ID
 * @param {constants.getSongByArtistOptions} options Optional Options
 * @param {String} token Genius' token
 * @returns {Promise<Song[]>} Song array
 */
function getSongByArtist (id, options, token) {
  return new Promise((resolve, reject) => {
    phin({
      url: `https://api.genius.com/artists/${id}/songs?sort=${options.sort}&per_page=${options.per_page}`,
      headers: { Authorization: 'Bearer ' + token },
      parse: 'json'
    })
      .then(res => {
        const songRaw = res.body.response.songs
        const songs = []

        songRaw.forEach(e => {
          songs.push(new Song(e))
        })

        resolve(songs)
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports = getSongByArtist
