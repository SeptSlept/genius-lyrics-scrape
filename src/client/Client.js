const constants = require('../util/constants.js')
const scrapeLyrics = require('./functions/scrapeLyrics.js')
const searchAPI = require('./functions/searchAPI.js')
const getArtist = require('./functions/getArtist.js')
const getSongByArtist = require('./functions/getSongByArtist.js')
const getSong = require('./functions/getSong.js')

// Structures
/* eslint-disable */
const SearchResult = require('../structures/SearchResult')
const Artist = require('../structures/Artist')
const Song = require('../structures/Song')
/* eslint-enable */

/**
 * The main hub for interacting with the Genius API
 */
class Client {
  /**
   * Constructs Client
   * @constructor
   * @param {String} token Genius API Access Token
   */
  constructor (token) {
    /**
     * Genius API Access Token
     * @type {String}
     */
    this.token = token
  }

  /**
   * Scrape Lyrics from Genius
   * @param {String} url Genius Page URL
   * @returns {Promise<String>} Lyrics
   */
  scrapeLyrics (url) {
    return scrapeLyrics(url)
  }

  /**
   * Searches Genius API for query. This also works for Genius URL query
   * @param {String} query
   * @returns {Promise<SearchResult>}
   */
  searchAPI (query) {
    return searchAPI(query, this.token)
  }

  /**
   * Gets Genius Artist info by ID
   * @param {Number} id The Artist's Genius ID
   * @returns {Promise<Artist>} Artist
   */
  getArtist (id) {
    return getArtist(id, this.token)
  }

  /**
   * Gets Song by ID
   * @param {Number} id The Song's Genius ID
   * @returns {Promise<Song>} Song
   */
  getSong (id) {
    return getSong(id, this.token)
  }

  /**
   * Gets the list of song created by the artist
   * @param {Number} id The Artist's Genius ID
   * @param {Object} [opts] Optional options
   * @returns {Promise<Song[]>}
   */
  getSongByArtist (id, opts) {
    opts = Object.assign(constants.getSongByArtistDefaultOpts, opts)
    return getSongByArtist(id, opts, this.token)
  }
}

module.exports = Client
