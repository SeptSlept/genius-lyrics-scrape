# Genius Lyrics Scraper

## Table of Contents
+ [About](#about)
+ [Getting Started](#getting_started)
+ [Features](#features)
+ [Example Usage](#usage)
+ [Documentation](#docs)
<!-- + [Contributing](../CONTRIBUTING.md) -->

## About <a name = "about"></a>
genius-lyrics-scrape is a Genius Lyrics API wrapper and a scraper.<br>

## Features <a name = "features"></a>
* Object-oriented
* Predictable abstractions
* Full CommonJS documentation (Intellisense)

## Getting Started <a name = "getting_started"></a>

### Installing

Add genius-lyrics-scrape to your Node.JS project

```
> npm i genius-lyrics-scrape
```

Require the module to your file

```js
const Genius = require('genius-lyrics-scrape')
```

## Example Usage <a name = "usage"></a>

```js
const Genius = require('genius-lyrics-scrape')
const token = require('YOUR-TOKEN-HERE')

const client = new Genius.Client(token)

client.searchAPI('Hello World')
  .then(searchRes => { console.log('First Result:\n', searchRes.result[0]) })
  .catch(err => console.err(err))

client.scrapeLyrics('https://genius.com/Travis-scott-sicko-mode-lyrics')
  .then(scraped => console.log('Scraped Lyrics:\n', scraped))
  .catch(err => console.err(err))
```

## Documentation <a name = "docs"></a>
See the documentation [here](https://septslept.gitlab.io/genius-lyrics-scrape/index)
